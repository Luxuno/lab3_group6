import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stat Compendium',
      home: appHome(),
    );
  }
}

class ClassCards extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ClassCardsState();
}

class ClassCardsState extends State<ClassCards> {
  List<int> KstatValues = [ 300, 150, 250, 80, 120, 70 ];
  List<int> WstatValues = [ 300, 290, 100, 50, 100, 50 ];
  List<int> PstatValues = [ 400, 175, 100, 100, 120, 55 ];
  List<int> NstatValues = [ 250, 180, 170, 190, 150, 100 ];
  List<bool> KBoosts = new List.filled(6, false);
  List<bool> WBoosts = new List.filled(6, false);
  List<bool> PBoosts = new List.filled(6, false);
  List<bool> NBoosts = new List.filled(6, false);
  List<int> Counters = [ 3, 4, 3, 3]; //Number of boosts each character can have


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: Center(
          child: Container(
            child: Column(
              children: [
                CardClass(
                  Image.asset('images/FF1_Knight.png'),
                  KstatValues,
                  'Knight',
                  KBoosts,
                  0,
                ),
                CardClass(
                  Image.asset('images/FF1_Wizard.png'),
                  WstatValues,
                  'Wizard',
                  WBoosts,
                  1,
                ),
                CardClass(
                  Image.asset('images/FF1_Priest.png'),
                  PstatValues,
                  'Priest',
                  PBoosts,
                  2,
                ),
                CardClass(
                  Image.asset('images/FF1_Ninja.png'),
                  NstatValues,
                  'Ninja',
                  NBoosts,
                  3,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container CardClass(Image imageAsset, List<int> stats, String className,
      List<bool> boosts, int countIndex) {
    List<String> statNames = [
      'HP',
      'MP',
      'Strength',
      'Agility',
      'Intelligence',
      'Luck'
    ];

    return Container(
      padding: EdgeInsets.all(15),
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)
        ),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                imageAsset,
                DataTable(
                  columnSpacing: 30,
                  dataRowHeight: 25,
                  headingRowHeight: 30,
                  columns: const <DataColumn>[
                    DataColumn(
                      label: Text(
                        'Stats',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        'Values',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                    ),
                  ],
                  rows: <DataRow>[
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[0])),
                        DataCell(Text(stats[0].toString())),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[1])),
                        DataCell(Text(stats[1].toString())),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[2])),
                        DataCell(Text(stats[2].toString())),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[3])),
                        DataCell(Text(stats[3].toString())),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[4])),
                        DataCell(Text(stats[4].toString())),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(statNames[5])),
                        DataCell(Text(stats[5].toString())),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Container(
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text('The ' + className,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
            ),

            //NEW CODE: Container at the end of each character's card on Tab 1
            //Each card gets 6 boost chips that the user can select, each character has
            //a certain number of boosts they can have applied at once. (For ex. Wizard uses his magic to get
            //4 boosts while everyone else only gets 3 boosts). When a chip is chosen, the
            //user is informed they have 1 less boost to choose and the relative stat gets updated.
            //Also, certain characters get an additional boost, so Wizard gets an extra +10 for Magicians touch,
            // Ninja an extra +10 for Gods speed, Knight gets an extra +10 for Hercules Strength,
            //and Priest gets an extra +10 for Healing hands boost.
            Container(
              padding: EdgeInsets.only(left: 20, bottom: 10, right: 20),
              child: Column(
                children: [
                  Center(
                    child: Text(
                        'Choose ${Counters[countIndex]} more boost(s): '),
                  ),
                  //Healing Hand boost chip and Magicians Touch boost chip.
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //chips for the boosts each character can use
                      InputChip(
                          label: Text('Healing Hand'),
                          backgroundColor: boosts[0] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.healing_rounded, color: Colors.white,)
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[0] == false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                    { boosts[0] = false; }
                                    else {
                                      if(className == "Priest")    //Priest gets a bonus for HP boost
                                        { stats[0] += 10; }
                                      Counters[countIndex] -= 1;
                                      stats[0] += 10;
                                      boosts[0] = true;
                                    }
                              }
                              else if (boosts[0] == true) {
                                if(className == "Priest")
                                { stats[0] -= 10; }
                                Counters[countIndex] += 1;
                                stats[0] -= 10;
                                boosts[0] = false;
                              }
                            });
                          }),
                      InputChip(
                          label: Text('Magicians Touch'),
                          backgroundColor: boosts[1] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.auto_awesome, color: Colors.white, size: 18,),
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[1] == false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                    { boosts[1] = false; }
                                    else {
                                      if(className == "Wizard")  //Wizard gets a bonus for the MP boost
                                      { stats[1] += 10; }
                                      Counters[countIndex] -= 1;
                                      stats[1] += 10;
                                      boosts[1] = true;
                                    }
                              }
                              else if (boosts[1] == true) {
                                if(className == "Wizard")
                                { stats[1] -= 10; }
                                Counters[countIndex] += 1;
                                stats[1] -= 10;
                                boosts[1] = false;
                              }
                            });
                          })
                    ],
                  ),
                  //Hercules Strength boost chip and Gods Speed boost chip
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InputChip(
                          label: Text('Hercules Strength'),
                          backgroundColor: boosts[2] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.fitness_center, color: Colors.white,),
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[2] == false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                      { boosts[2] = false; }
                                    else {
                                      if(className == "Knight")  //Knight gets a bonus for the strength boost
                                      { stats[2] += 10; }
                                      Counters[countIndex] -= 1;
                                      stats[2] += 10;
                                      boosts[2] = true;
                                    }
                              }
                              else if (boosts[2] == true) {
                                if(className == "Knight")
                                { stats[2] -= 10; }
                                Counters[countIndex] += 1;
                                stats[2] -= 10;
                                boosts[2] = false;
                              }
                            });
                          }),
                      InputChip(
                          label: Text('Gods Speed'),
                          backgroundColor: boosts[3] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.directions_run, color: Colors.white,),
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[3] == false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                    { boosts[3] = false; }
                                    else {
                                      if(className == "Ninja")  //Ninja gets a bonus for the agility boost
                                      { stats[3] += 10; }
                                      Counters[countIndex] -= 1;
                                      stats[3] += 10;
                                      boosts[3] = true;
                                    }
                              }
                              else if (boosts[3] == true) {
                                if(className == "Ninja")
                                { stats[3] -= 10; }
                                Counters[countIndex] += 1;
                                stats[3] -= 10;
                                boosts[3] = false;
                              }
                            });
                          })
                    ],
                  ),
                  //Practiced Mind (Intelligence) boost chip and Lucky Fellow boost chip
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InputChip(
                          label: Text('Practiced Mind'),
                          backgroundColor: boosts[4] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.psychology, color: Colors.white,),
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[4] == false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                    { boosts[4] = false; }
                                    else {
                                      Counters[countIndex] -= 1;
                                      stats[4] += 10;
                                      boosts[4] = true;
                                    }
                              }
                              else if (boosts[4] == true) {
                                Counters[countIndex] += 1;
                                stats[4] -= 10;
                                boosts[4] = false;
                              }
                            });
                          }),
                      InputChip(
                          label: Text('Lucky Fellow'),
                          backgroundColor: boosts[5] ? Colors.blue : Colors
                              .grey,
                          avatar: CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(Icons.casino, color: Colors.white ),
                          ),
                          onPressed: () {
                            setState(() {
                              if (boosts[5] ==
                                  false) //we have not applied the boost
                                  {
                                    if(Counters[countIndex] == 0)
                                    { boosts[5] = false; }
                                    else {
                                      Counters[countIndex] -= 1;
                                      stats[5] += 10;
                                      boosts[5] = true;
                                    }
                              }
                              else if (boosts[5] == true) {
                                Counters[countIndex] += 1;
                                stats[5] -= 10;
                                boosts[5] = false;
                              }
                            });
                          })
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

enum Levels {BarenFalls, DomaCastle, PhantomForest}

class LocationCards extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LocationCardsState();
}

//having an issue with the dialogue option not refreshing after choosing the level
//you have to click the button again in order for the page to update
class LocationCardsState extends State<LocationCards> {
  String levelImage = 'images/BarenFalls.png';
  String levelName = 'Baren Falls';
  String FlavorText = 'The first location you find in the game. A lush set of islands '
      'surrounded by a huge waterfall. The Priest has a '
      'vision once they reach the peak of the hill, warning them of the impending '
      'invasion on their home town.';

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: Center(
          child: Container(
            child: Column(
              children: [
                RaisedButton(
                  child: Text('Choose level'),
                    onPressed: () {
                  setState(() {
                    chooseLevel();
                  });
                }),
            CardLocation(levelImage,levelName),
              ],
            ),
          ),
        ),
      ),
    );
  }

  static Chip itemChip(String itemName) {
    return Chip(
      label: Text(itemName),
      backgroundColor: Colors.grey[400],
    );
  }

  List<Chip> barrenFallsItems = [
    itemChip("Lute"),
    itemChip("Potion"),
    itemChip("Sword"),
    itemChip("Key"),
    itemChip("Sword"),
    itemChip("TNT"), ];
  List<Chip> domaCastleItems = [
    itemChip("Bottle"),
    itemChip("Potion"),
    itemChip("Key"),
    itemChip("Rod"),
    itemChip("Chime"), ];
  List<Chip> phantomForestItems = [
    itemChip("Crystal"),
    itemChip("Potion"),
    itemChip("Key"),
    itemChip("Herb"),
    itemChip("Chime"),
    itemChip("Bottle"),
    itemChip("Rod"),
    itemChip("Ruby"),
    itemChip("Tail"),
    itemChip("Slab"), ];

  List<Chip> itemChips = new List<Chip>();


  Future<void> chooseLevel() async {
    switch (await showDialog<Levels>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(title: const Text('Select level'),
          children: <Widget>[
            SimpleDialogOption(onPressed: () {
              Navigator.pop(context, Levels.BarenFalls);
                setState(() { itemChips = barrenFallsItems; });
              },
              child: const Text('Baren Falls'),
            ),
            SimpleDialogOption(onPressed: () {
              Navigator.pop(context, Levels.DomaCastle);
                setState(() { itemChips = domaCastleItems;  });
                },
              child: const Text('Doma Castle'),
            ),
            SimpleDialogOption(onPressed: () {
              Navigator.pop(context, Levels.PhantomForest);
              setState(() { itemChips = phantomForestItems;  });
              },
              child: const Text('Phantom Forest'),
            )
          ],
        );
      })) {
      case Levels.BarenFalls:
        levelImage = 'images/BarenFalls.png';
        levelName = 'Baren Falls';
        FlavorText = 'The first location you find in the game. A lush set of islands '
            'surrounded by a huge waterfall. The Priest has a '
            'vision once they reach the peak of the hill, warning them of the impending '
            'invasion on their home town.';

        break;
      case Levels.DomaCastle:
        levelImage = 'images/DomaCastle.png';
        levelName = 'Doma Castle';
        FlavorText = 'The second location you find in the game. An abandoned castle '
            'that once held back the first invasion of the Geth. The Wizard is found here'
            ' searching for the lost texts that they believe contains vital clues to '
            'defeating the Geth once and for all.';
        break;
      case Levels.PhantomForest:
        levelImage = 'images/PhantomForest.png';
        levelName = 'Phantom Forest';
        FlavorText = 'The third location you find in the game. A forest said to be the'
            ' main means of connecting the living world and the spirit world. The ninja is'
            ' found here searching and trying to figure out a way to transfer over to the '
            'spirit world.';
        break;
  }
    }

  Container CardLocation(String imageAsset, String LocName) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)
        ),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(imageAsset)
              ],
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text(LocName,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text(FlavorText,
                style: TextStyle(fontSize: 14),),
            ),
            Divider(
              color: Colors.grey,
              thickness: 1, indent: 50, endIndent: 50,
            ),
            Container(child: Text('Items',), alignment: Alignment.center,),
            Wrap(
              direction: Axis.horizontal,
              spacing: 10,
              children: itemChips.isEmpty ? barrenFallsItems : itemChips,
            ),
          ],
        ),
      ),
    );
  }
}

class appHome extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => appHomeState();
}

class appHomeState extends State<appHome> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(      //introduced a tab view
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: TabBar(
           labelColor: Colors.purple,
            indicatorColor: Colors.purple,
            tabs: [
              Tab(text: 'Characters'),
              Tab(text: 'Locations'),
          ],
        ),
        ),
        body: TabBarView(
          children: [         //Each child here is the actual tab content so, 2 tabs means no more or less than 2 children
            ClassCards(),     //Tab 1
            LocationCards()  //Tab 2
          ],
        ),
      ),
    );
  }
}